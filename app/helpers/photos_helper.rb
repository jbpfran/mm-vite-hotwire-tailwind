module PhotosHelper
  def photos_list(folder)
    Dir.glob("source/images/galleries/" + folder + "/**/*.{jpg,jpeg,JPG}").sort
  end
  
  def albums_list
    Dir.glob("source/images/galleries/*").sort
  end
  
  def album_thumbnail(folder)
    Dir.glob(folder + "/**/*.{jpg,jpeg,JPG}").sort.first.delete_prefix("source/images/")
  end
  
  def album_cover_page(folder)
    folder.split('/').last + '-cover'
  end
  
  def album_cover_photo(folder)
    Dir.glob("source/images/galleries/" + folder + "/**/*.{jpg,jpeg,JPG}").sort.first.delete_prefix("source/images/")
  end
  
  def album_photos(folder)
    folder.split('/').last + '-photos'
  end
  
  def album_photos_frame(folder)
    folder.split('/').last + '-frame'
  end
  
  def previous_photo_page(album, current_photo)
    album_photos = Photo.list(album).sort
    current_index = album_photos.index(current_photo)
    previous_photo = album_photos[current_index - 1]
    '/album/' + previous_photo.delete_prefix("source/images/galleries/").gsub(/(jpg|jpeg|JPG)/, 'html')
  end
  
  def next_photo_page(album, current_photo)
    album_photos = Photo.list(album).sort
    current_index = album_photos.index(current_photo)
    if album_photos[current_index + 1]
      next_photo = album_photos[current_index + 1]
    else
      next_photo = album_photos[0]
    end
    '/album/' + next_photo.delete_prefix("source/images/galleries/").gsub(/(jpg|jpeg|JPG)/, 'html')
  end
  
  def photo_path(photo, size = "")
    if size.empty?
      return photo.delete_prefix("source/images/")
    end
    image_path(photo.delete_prefix("source/images/"), resize: size)
  end
  
  def photo_view_tag(photo)
    
  end
  
  def album_name(folder)
    folder.delete_prefix("source/images/galleries/").gsub("_", " ")
  end
end